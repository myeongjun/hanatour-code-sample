# API DOCS

## CREATE

입력으로 스크랩할 이미지의 URL과 이미지에 대한 제목 및 설명을 받아서 DB에 저장 하고 입력된 이미지 URL을 다운로드 하여 로컬 파일시스템에 저장

**URL** : `/create/`

**Method** : `POST`

**Data constraints**

```json
{
    "title": "[1 ~ 500자]",
    "description": "[1 ~ 65,535자]",
    "url": "[url 형식]"
}
```

**Data examples**

```json
{
    "title": "가나다",
    "description": "사진설명",
    "url": "https://cdn.shopify.com/s/files/1/0272/4770/6214/articles/when-do-puppies-start-walking.jpg"
}
```

## Success Responses

**Code** : `200 OK`

```json
{
    "title": "가나다"
}
```

---

## MODIFY

내가 생성한 스크랩 데이터의 제목 및 설명 수정

**URL** : `/image/:imageId`

**Method** : `PUT`

**Data constraints**

```json
{
    "title": "[1 ~ 500자]",
    "description": "[1 ~ 65,535자]"
}
```

**Data examples**

```json
{
    "title": "제목 수정 중",
    "description": "내용 수정 중"
}
```

## Success Responses

**Code** : `200 OK`

```json
{
    "filename": "http://localhost:8090/download/71668c4e-0163-419e-932c-b1b9df68c8f6.jpg",
    "title": "제목 수정 중",
    "description": "내용 수정 중"
}
```

---

## REMOVE

내가 생성한 스크랩 데이터 삭제

**URL** : `/image/:imageId`

**Method** : `REMOVE`

## Success Responses

**Code** : `200 OK`

```json
{
    "id": 1
}
```

---

## LIST

생성된 모든 사용자의 스크랩 데이터 목록. 목록은 Pagination을 고려(page / size 생략 시, page: 0, size: 100 기본값). 목록에는 로컬 서버에 저장한 이미지에 대한 썸네일 URL과 제목, 조회수를 응답

**URL** : `/images?page=0&size=100`

**Method** : `GET`

## Success Responses

**Code** : `200 OK`

```json
[
    {
        "filename": "http://localhost:8090/download/95953bbb-1fac-4864-b6f9-f7a7bd72d6e6.jpg",
        "title": "가나다",
        "views": 0
    },
    {
        "filename": "http://localhost:8090/download/f401c619-d070-49a4-be93-e402e9485708.jpg",
        "title": "가나다",
        "views": 0
    }
]
```

---

## GET

생성한 스크랩 데이터 1개의 상세 정보. 로컬 서버에 저장한 이미지 URL과 제목, 설명을 응답 하고 조회수를 증가

**URL** : `/image/:imageId`

**Method** : `GET`

## Success Responses

**Code** : `200 OK`

```json
{
    "filename": "http://localhost:8090/download/95953bbb-1fac-4864-b6f9-f7a7bd72d6e6.jpg",
    "title": "가나다",
    "description": "사진설명"
}
```

---

## DOWNLOAD

이미지 파일 다운로드

**URL** : `/download/:filename`

**Method** : `GET`

## Success Responses

**Desc** : 파일 다운로드

---

## Error Response

에러 발생 시 200 OK 로 응답을 내려주며, 에러 구분은 코드로 구분.

**Code** : `200 OK`

**Content example** :

```json
{
    "errorCode": "IMAGE_NOT_EXIST",
    "errorMessage": "이미지가 존재하지 않습니다"
}
```
