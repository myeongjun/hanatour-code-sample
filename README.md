# Scraper

이미지 스크랩을 위한 서버. Image URL을 입력 받아서 스크랩한 데이터를, Scraper 서버를 통해서 다른 사용자들이 볼 수 있습니다.

---

## 환경구성

- Java 1.8
- Spring Boot 2.7.1
- H2 DB (schema script: [docs/db_schema.sql](docs/db_schema.sql))
- 파일 저장 경로: `/home/mj/work/temp/scraper_temp/` ([application.yaml](src/main/resources/application.yaml) 에서 수정 가능)
- Rabbit MQ (`CREATE_IMAGE_QUEUE` 큐 생성 필요)
- Redis

---

## API 엔드포인트

* [create](docs/api_docs.md#CREATE) : `POST /create/`
* [modify](docs/api_docs.md#MODIFY) : `PUT /image/:imageId`
* [remove](docs/api_docs.md#REMOVE) : `DELETE /image/:imageId`
* [list](docs/api_docs.md#LIST) : `GET /images/`
* [get](docs/api_docs.md#GET) : `GET /image/:imaegId`
* [download](docs/api_docs.md#DOWNLOAD): `/download/:filename`

---

## 개발 컨셉

![system-diagram](docs/system-diagram.png)

- 유저: 별도 유저 관리를 하지 않기 때문에 유저 구분은 IP로 구분.
- DB: 현재는 이미지Id(pk)로 조회하기 때문에 인덱스 불필요.
- 다수 인원이 동시에 스크랩(생성): 
    1) 다수의 유저가 스크랩(생성)을 할 시, DB INSERT 부하가 생겨 큐(Rabbit MQ)에 요청을 저장해 놓고 처리. (DB 처리 보다 큐에 요청을 넣는 작업이 부하가 적음)
    2) 개선 후 creat api의 스트레스 테스트: [위치](docs/test)
- 다수 인원이 동시에 조회:
    1) 다수의 인원이 DB에 데이터 조회를 하는 것이 아닌, 메모리 저장소(Redis)를 사용하여 캐시 처리
    2) 조회수 증가할 때마다 UPDATE를 한다면 부하가 발생하기 때문에, 조회수 증가 값을 Redis 에서 관리 후 스케쥴(일괄)을 통한 (게시물 별로) 1회 UPDATE.
