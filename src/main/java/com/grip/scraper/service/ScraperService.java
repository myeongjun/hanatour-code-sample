package com.grip.scraper.service;

import com.grip.scraper.domain.Image;
import com.grip.scraper.domain.ImageRepository;
import com.grip.scraper.dto.*;
import com.grip.scraper.exception.ScraperErrorCode;
import com.grip.scraper.exception.ScraperException;
import com.grip.scraper.util.FileStore;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class ScraperService {

    private final ImageRepository imageRepository;
    private final FileStore fileStore;
    private final RedisTemplate<String, Object> redisTemplate;

    private final static String PREFIX_VIEWS_COUNT = "viewCount:";

    /**
     * 스크랩 데이터 생성 시 즉시 Insert 하지 않고, 큐에 저장 후 Insert 처리(위치: queue/Consumer)
     * 요청을 받았을 때는 유효성 검사만 진행
     *
     * @param request
     * @return
     */
    public CreateImageDto.Response createImage(CreateImageDto.Request request) {
        validateCreateImage(request);
        return new CreateImageDto.Response(request.getTitle());
    }

    private void validateCreateImage(CreateImageDto.Request request) {
        // 이미지 파일 확장자 확인
        String [] imageFileExt = {"jpg", "gif", "png", "jpeg", "bmp"};
        String requestFileExt = fileStore.extractExt(request.getUrl());

        if (!Arrays.stream(imageFileExt).anyMatch(requestFileExt::equals)) {
            throw new ScraperException(ScraperErrorCode.IMAGE_FILE_KIND_ERROR);
        }
    }

    public List<ImageListDto> getAllImages(Integer page, Integer size) {
        return imageRepository
                .findByIsRemovedEquals(false, PageRequest.of(page, size, Sort.by("id")))
                .stream()
                .map(ImageListDto::new)
                .collect(Collectors.toList());
    }

    @Cacheable(value = "ImageDetailDto", key="#imageId", cacheManager = "cacheManager")
    @Transactional
    public ImageDetailDto getImage(String imageId) {
        Image image = imageRepository.findByIdAndIsRemoved(Long.valueOf(imageId), false)
                .orElseThrow(() -> new ScraperException(ScraperErrorCode.IMAGE_NOT_EXIST));

        return new ImageDetailDto(image);
    }

    /**
     * 방문자 조회수 증가, DB에 바로 적용 하지 않고 redis에 값 증가 -> 이후 아래 updateAddViewsCount 에서 일괄 적용
     *
     * @param imageId
     */
    public void addViewsCount(String imageId) {
        String viewsCountKey = PREFIX_VIEWS_COUNT + imageId;
        ValueOperations<String, Object> ops = redisTemplate.opsForValue();

        // 최초 존재 하지 않을 때 1 초기화
        Boolean notExist = ops.setIfAbsent(viewsCountKey, 1);

        if (!notExist) {
            // 존재 한다면 +1
            int value = (Integer) ops.get(viewsCountKey) + 1;
            ops.set(viewsCountKey, value);
        }
    }

    @Transactional
    @Scheduled(cron = "*/3 * * * * *")
    public void updateAddViewsCount() {
        Set<String> keys = redisTemplate.keys(PREFIX_VIEWS_COUNT + "*");
        ValueOperations<String, Object> ops = redisTemplate.opsForValue();

        keys.forEach(key -> {
            String imageId = key.split(":")[1];
            Integer viewsCount = (Integer) ops.getAndDelete(key);
            Image image = imageRepository.findByIdAndIsRemoved(Long.valueOf(imageId), false)
                    .orElseThrow(() -> new ScraperException(ScraperErrorCode.IMAGE_NOT_EXIST));
            image.updateViews(viewsCount);
        });
    }

    private Image getImageByIdAndUserId(String imageId, String userId) {
        return imageRepository
                .findByIdAndUserIdAndIsRemoved(Long.valueOf(imageId), userId, false)
                .orElseThrow(() -> new ScraperException(ScraperErrorCode.IMAGE_NOT_EXIST));
    }

    @Transactional
    public EditImageDto.Response editImage(String imageId, EditImageDto.Request request, String userId) {
        Image image = getImageByIdAndUserId(imageId, userId);
        image.update(request.getTitle(), request.getDescription());

        return new EditImageDto.Response(image);
    }

    @Transactional
    public DeleteImageDto deleteImage(String imageId, String userId) {
        Image image = getImageByIdAndUserId(imageId, userId);
        image.update(true);

        return new DeleteImageDto(image);
    }
}
