package com.grip.scraper.exception;

import com.grip.scraper.dto.ScraperErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

import static com.grip.scraper.exception.ScraperErrorCode.*;

@Slf4j
@ControllerAdvice
public class ScraperExceptionHandler {

    @ResponseBody
    @ExceptionHandler(ScraperException.class)
    public ScraperErrorResponse handleException(ScraperException e, HttpServletRequest request) {
        log.error("Error Code: {}, url: {}, message: {}"
                , e.getScraperErrorCode()
                , request.getRequestURI()
                , e.getDetailMessage()
        );

        return ScraperErrorResponse.builder()
                .errorCode(e.getScraperErrorCode())
                .errorMessage(e.getDetailMessage())
                .build();
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {
            HttpRequestMethodNotSupportedException.class,
            MethodArgumentNotValidException.class
    })
    public ScraperErrorResponse handleBadRequest(Exception e, HttpServletRequest request) {
        log.error("url: {}, message: {}", request.getRequestURI(), e.getMessage(), e);

        return ScraperErrorResponse.builder()
                .errorCode(INVALID_REQUEST)
                .errorMessage(INVALID_REQUEST.getMessage())
                .build();
    }

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public ScraperErrorResponse handleException(Exception e, HttpServletRequest request) {
        log.error("url: {}, message: {}", request.getRequestURI(), e.getMessage(), e);

        return ScraperErrorResponse.builder()
                .errorCode(INTERNAL_SERVER_ERROR)
                .errorMessage(INTERNAL_SERVER_ERROR.getMessage())
                .build();
    }
}
