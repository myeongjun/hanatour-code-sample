package com.grip.scraper.exception;

import lombok.Getter;

@Getter
public class ScraperException extends RuntimeException {

    private ScraperErrorCode scraperErrorCode;
    private String detailMessage;

    public ScraperException(ScraperErrorCode errorCode) {
        super(errorCode.getMessage());
        this.scraperErrorCode = errorCode;
        this.detailMessage = errorCode.getMessage();
    }

    public ScraperException(ScraperErrorCode errorCode, String detailMessage) {
        super(detailMessage);
        this.scraperErrorCode = errorCode;
        this.detailMessage = detailMessage;
    }
}
