package com.grip.scraper.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ScraperErrorCode {
    IMAGE_NOT_EXIST("이미지가 존재하지 않습니다"),
    IMAGE_FILE_KIND_ERROR("이미지 파일만 다운로드 할 수 있습니다."),
    IMAGE_STORE_ERROR("이미지 저장중 문제가 발생하였습니다."),
    INTERNAL_SERVER_ERROR("서버에 문제가 발생했습니다."),
    INVALID_REQUEST("잘못된 요청입니다.");

    private final String message;
}
