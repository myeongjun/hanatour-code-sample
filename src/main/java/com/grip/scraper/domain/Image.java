package com.grip.scraper.domain;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 스크랩 이미지 관리 테이블
 *
 * - views, isRemoved 컬럼 기본값 변경이 필요 하다면 PrePersist 사용 고려
 * (현재는 기본값 변경이 안될 것으로 판단, 원시타입으로 직접 지정)
 * - 유저 관리 기능 없기 때문에 현재는 유저 ID는 요청 IP로 저장
 *
 */
@Getter
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Entity
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;                                    // ID (key)

    @Column(length = 500, nullable = false)
    private String title;                               // 제목

    @Column(columnDefinition = "TEXT")
    private String description;                         // 설명

    @Column(length = 2083, nullable = false)
    private String url;                                 // 이미지 URL

    @Column(nullable = false)
    private String filename;                            // 저장될 때의 파일 이름

    private Integer views = 0;                          // 조회수 (기본값 0)

    private Boolean isRemoved = false;                  // 삭제 여부 (기본값 false)

    @CreatedDate
    private LocalDateTime createdAt;                    // 생성 날짜

    @LastModifiedDate
    private LocalDateTime updatedAt;                    // 수정 날짜

    @Column(length = 50)
    private String userId;                              // 유저 Id (유저 관리가 없기 때문에 현재는 IP로 구분)

    @Builder
    public Image(String title, String description, String url, String filename, String userId) {
        this.title = title;
        this.description = description;
        this.url = url;
        this.filename = filename;
        this.userId = userId;
    }

    /**
     * Image 수정을 위한 메소드
     *
     * @param title 수정을 위한 제목
     * @param description 수정을 위한 설명
     */
    public void update(String title, String description) {
        this.title = title;
        this.description = description;
    }

    /**
     * Image 삭제를 위한 메소드
     * (실제 데이터 삭제가 아닌, 삭제여부는 플래그로 관리)
     *
     * @param isRemoved 삭제 여부(삭제 시 true 입력)
     */
    public void update(Boolean isRemoved) {
        this.isRemoved = isRemoved;
    }

    /**
     * Image 조회수(views) 증가
     */
    public void updateViews(Integer views) {
        this.views += views;
    }
}
