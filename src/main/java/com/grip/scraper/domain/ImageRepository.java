package com.grip.scraper.domain;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ImageRepository extends JpaRepository<Image, Long> {

    Optional<Image> findByIdAndIsRemoved(Long id, Boolean isRemoved);

    Optional<Image> findByIdAndUserIdAndIsRemoved(Long id, String userId, Boolean isRemoved);

    List<Image> findByIsRemovedEquals(Boolean isRemoved, Pageable pageable);
}
