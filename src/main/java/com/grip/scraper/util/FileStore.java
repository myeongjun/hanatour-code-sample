package com.grip.scraper.util;

import com.grip.scraper.exception.ScraperErrorCode;
import com.grip.scraper.exception.ScraperException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Component
public class FileStore {

    @Value("${file.dir}")
    private String fileDir;

    @Value("${download.base.url}")
    private String url;

    public String getFullPath(String filename) {
        return fileDir + filename;
    }

    public String storeFile(String imageUrl) {

        String storeFileName = createStoreFileName(imageUrl);

        try (InputStream is = new URL(imageUrl).openStream()) {
            Path storePath = Paths.get(getFullPath(storeFileName));
            Files.copy(is, storePath);
        } catch (Exception e) {
            throw new ScraperException(ScraperErrorCode.IMAGE_STORE_ERROR);
        }

        return url + storeFileName;
    }

    public String createStoreFileName(String originalFilename) {
        String ext = extractExt(originalFilename);
        String uuid = UUID.randomUUID().toString();
        return uuid + "." + ext;
    }

    public String extractExt(String originalFilename) {
        int pos = originalFilename.lastIndexOf(".");
        return originalFilename.substring(pos + 1);
    }
}
