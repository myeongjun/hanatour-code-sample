package com.grip.scraper.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.grip.scraper.config.argumentresolver.Login;
import com.grip.scraper.dto.*;
import com.grip.scraper.service.ScraperService;
import com.grip.scraper.util.FileStore;
import com.grip.scraper.queue.Producer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriUtils;

import javax.validation.Valid;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
public class ScraperController {

    private final ScraperService scraperService;
    private final FileStore fileStore;
    private final Producer producer;
    private final ObjectMapper objectMapper;

    @GetMapping("/test")
    public String test() {
        return "hello";
    }

    /**
     * [create] - 스크랩 데이터 생성, 스크랩 대상 URL을 받고 로컬에 이미지 저장
     *
     * @param request URL, 제목, 설명
     * @param userId 유저 IP (서버 내에서 생성 인자)
     * @return
     */
    @PostMapping("/create")
    public CreateImageDto.Response createImage(
            @Valid @RequestBody CreateImageDto.Request request,
            @Login final String userId
    ) throws JsonProcessingException {
        // 파일 저장 및 DB 작업은 큐에 입력 후 처리
        String jsonRequest = objectMapper.writeValueAsString(
                new HashMap<String, Object>() {{
                  put("request", request);
                  put("userId", userId);
                }}
        );
        producer.sendTo(jsonRequest);

        return scraperService.createImage(request);
    }

    /**
     * [list] - 생성된 모든 스크랩 데이터 리스트
     * (페이징 디폴트 - page: 0, size: 100)
     *
     * @param page 페이지 번호
     * @param size 화면에 보이는 리스트 개수
     * @return
     */
    @GetMapping("/images")
    public List<ImageListDto> getAllImages(
            @RequestParam(value="page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "100") Integer size
    ) {
        return scraperService.getAllImages(page, size);
    }

    /**
     * [get] - 스크랩 데이터 상세 정보
     *
     * @param imageId 스크랩 ID
     * @return
     */
    @GetMapping("/image/{imageId}")
    public ImageDetailDto getImage(
            @PathVariable final String imageId
    ) {
        scraperService.addViewsCount(imageId);
        return scraperService.getImage(imageId);
    }

    /**
     * [modify] - 스크랩 데이터 수정
     *
     * @param imageId 스크랩 ID
     * @param request 수정 내용
     * @param userId 유저 IP (서버 내에서 생성 인자)
     * @return
     */
    @PutMapping("/image/{imageId}")
    public EditImageDto.Response editImage(
            @PathVariable final String imageId,
            @Valid @RequestBody EditImageDto.Request request,
            @Login final String userId
    ) {
        return scraperService.editImage(imageId, request, userId);
    }

    /**
     * [remove] - 스크랩 데이터 삭제
     *
     * @param imageId 스크랩 ID
     * @param userId 유저 IP (서버 내에서 생성 인자)
     * @return
     */
    @DeleteMapping("/image/{imageId}")
    public DeleteImageDto deleteImage(
            @PathVariable final String imageId,
            @Login final String userId
    ) {
        return scraperService.deleteImage(imageId, userId);
    }

    /**
     * 파일 다운로드
     *
     * @param filename 파일 이름
     * @return
     * @throws MalformedURLException
     */
    @GetMapping("/download/{filename}")
    public ResponseEntity<Resource> downloadImage(
            @PathVariable final String filename
    ) throws MalformedURLException {
        UrlResource resource = new UrlResource("file:" + fileStore.getFullPath(filename));
        String encodedUploadFileName = UriUtils.encode(filename, StandardCharsets.UTF_8);
        String contentDisposition = "attachment; filename=\"" + encodedUploadFileName+ "\"";

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, contentDisposition)
                .body(resource);
    }
}
