package com.grip.scraper.dto;

import com.grip.scraper.domain.Image;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Getter
public class ImageDetailDto implements Serializable {
    private String filename;
    private String title;
    private String description;

    public ImageDetailDto(Image entity) {
        this.filename = entity.getFilename();
        this.title = entity.getTitle();
        this.description = entity.getDescription();
    }
}
