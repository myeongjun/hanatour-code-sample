package com.grip.scraper.dto;

import com.grip.scraper.exception.ScraperErrorCode;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ScraperErrorResponse {
    private ScraperErrorCode errorCode;
    private String errorMessage;
}
