package com.grip.scraper.dto;

import com.grip.scraper.domain.Image;
import lombok.Getter;

@Getter
public class DeleteImageDto {

    private Long id;

    public DeleteImageDto(Image entity) {
        this.id = entity.getId();
    }
}
