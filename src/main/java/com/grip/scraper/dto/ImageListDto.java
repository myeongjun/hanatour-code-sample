package com.grip.scraper.dto;


import com.grip.scraper.domain.Image;
import lombok.Getter;

@Getter
public class ImageListDto {
    private String filename;
    private String title;
    private Integer views;

    public ImageListDto(Image entity) {
        this.filename = entity.getFilename();
        this.title = entity.getTitle();
        this.views = entity.getViews();
    }
}
