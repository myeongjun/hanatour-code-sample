package com.grip.scraper.dto;

import com.grip.scraper.domain.Image;
import lombok.*;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class EditImageDto {

    @Getter
    public static class Request {

        @Size(min = 1, max = 500, message = "제목 길이는 1 ~ 500 입니다.")
        @NotNull
        private String title;

        @NotNull
        private String description;

        @Builder
        public Request(String title, String description) {
            this.title = title;
            this.description = description;
        }
    }

    @Getter
    public static class Response {
        private String filename;
        private String title;
        private String description;

        public Response(Image entity) {
            this.filename = entity.getFilename();
            this.title = entity.getTitle();
            this.description = entity.getDescription();
        }
    }
}
