package com.grip.scraper.dto;

import com.grip.scraper.domain.Image;
import lombok.*;
import org.hibernate.validator.constraints.URL;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateImageDto {

    @ToString
    @Getter
    @Builder
    public static class Request {

        @Size(min = 1, max = 500, message = "제목 길이는 1 ~ 500 입니다.")
        @NotNull
        private String title;

        private String description;

        @URL(message = "URL 형식을 필요합니다.")
        @Size(min = 1, max = 2083, message = "url 길이는 1 ~ 2083 입니다.")
        @NotNull
        private String url;

        public Image toEntity(String filename, String userId) {
            return Image.builder()
                    .title(title)
                    .description(description)
                    .url(url)
                    .filename(filename)
                    .userId(userId)
                    .build();
        }
    }

    @Getter
    @Builder
    public static class Response {
        private String title;

        public Response(String title) {
            this.title = title;
        }
    }
}
