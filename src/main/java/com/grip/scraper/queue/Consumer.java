package com.grip.scraper.queue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.grip.scraper.domain.ImageRepository;
import com.grip.scraper.dto.CreateImageDto;
import com.grip.scraper.util.FileStore;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
@Component
public class Consumer {

    private final ImageRepository imageRepository;
    private final FileStore fileStore;
    private final ObjectMapper objectMapper;

    @RabbitListener(queues = "CREATE_IMAGE_QUEUE")
    public void createImageHandler(String message) throws IOException {
        Map requestMap = objectMapper.readValue(message, HashMap.class);
        CreateImageDto.Request request = objectMapper.convertValue(
                requestMap.get("request"),
                CreateImageDto.Request.class
        );
        String userId = (String) requestMap.get("userId");

        // 이미지 파일 저장 및 DB에 데이터 저장
        String storeFileName = fileStore.storeFile(request.getUrl());
        imageRepository.save(request.toEntity(storeFileName, userId));
    }
}
