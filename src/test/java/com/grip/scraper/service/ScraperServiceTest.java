package com.grip.scraper.service;

import com.grip.scraper.domain.Image;
import com.grip.scraper.domain.ImageRepository;
import com.grip.scraper.dto.*;
import com.grip.scraper.exception.ScraperErrorCode;
import com.grip.scraper.exception.ScraperException;
import com.grip.scraper.util.FileStore;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class ScraperServiceTest {

    @Mock
    private ImageRepository imageRepository;

    @Mock
    private FileStore fileStore;

    @InjectMocks
    private ScraperService scraperService;

    @Test
    void createImageTest_success() {
        // given
        String title = "Test Title";
        String url = "https://cdn.shopify.com/s/files/1/0272/4770/6214/articles/when-do-puppies-start-walking.jpg";

        CreateImageDto.Request request = CreateImageDto.Request.builder()
                .title(title)
                .description("Test Description")
                .url(url)
                .build();
        given(fileStore.extractExt(url))
                .willReturn("jpg");

        // when
        CreateImageDto.Response response = scraperService.createImage(request);

        // then
        assertEquals(title, response.getTitle());
    }

    @Test
    void createImageTest_failed_with_ext() {
        // given
        String title = "Test Title";
        String url = "https://cdn.shopify.com/s/files/1/0272/4770/6214/articles/memo.txt";

        CreateImageDto.Request request = CreateImageDto.Request.builder()
                .title(title)
                .description("Test Description")
                .url(url)
                .build();
        given(fileStore.extractExt(url))
                .willReturn("txt");

        // when
        // then
        ScraperException exception = assertThrows(ScraperException.class,
                () -> scraperService.createImage(request));
        assertEquals(ScraperErrorCode.IMAGE_FILE_KIND_ERROR, exception.getScraperErrorCode());
    }

    @Test
    void getAllImages_success() {
        // given
        String title = "Test Title";
        String filename = "Test Filename";
        String url = "https://cdn.shopify.com/s/files/1/0272/4770/6214/articles/when-do-puppies-start-walking.jpg";

        Image image = Image.builder()
                .title(title)
                .description("Test Description")
                .filename(filename)
                .url(url)
                .userId("test user id")
                .build();
        given(imageRepository.findByIsRemovedEquals(anyBoolean(), any(PageRequest.class)))
                .willReturn(Arrays.asList(image));

        // when
        List<ImageListDto> imageListDto = scraperService.getAllImages(0, 100);

        // then
        ImageListDto dto = imageListDto.get(0);
        assertEquals(title, dto.getTitle());
        assertEquals(0, dto.getViews());
        assertEquals(filename, dto.getFilename());
    }

    @Test
    void getImage_success() {
        // given
        String title = "Test Title";
        String description = "Test Description";
        String filename = "Test Filename";
        String url = "https://cdn.shopify.com/s/files/1/0272/4770/6214/articles/when-do-puppies-start-walking.jpg";

        Image image = Image.builder()
                .title(title)
                .description(description)
                .filename(filename)
                .url(url)
                .userId("test user id")
                .build();

        given(imageRepository.findByIdAndIsRemoved(anyLong(), anyBoolean()))
                .willReturn(Optional.of(image));

        // when
        ImageDetailDto dto = scraperService.getImage("0");

        // then
        assertEquals(description, dto.getDescription());
        assertEquals(filename, dto.getFilename());
        assertEquals(title, dto.getTitle());
    }

    @Test
    void editImage_success() {
        // given
        String title = "Test Title";
        String description = "Test Description";
        String filename = "Test Filename";
        String url = "https://cdn.shopify.com/s/files/1/0272/4770/6214/articles/when-do-puppies-start-walking.jpg";

        Image image = Image.builder()
                .title(title)
                .description(description)
                .filename(filename)
                .url(url)
                .userId("test user id")
                .build();

        given(imageRepository.findByIdAndUserIdAndIsRemoved(anyLong(), anyString(), anyBoolean()))
                .willReturn(Optional.of(image));

        String editDescription = "Edit Description";
        String editTitle = "Edit title";
        EditImageDto.Request request = EditImageDto.Request.builder()
                .description(editDescription)
                .title(editTitle)
                .build();

        // when
        EditImageDto.Response dto = scraperService.editImage("0", request, "userId");

        // then
        assertEquals(editDescription, dto.getDescription());
        assertEquals(editTitle, dto.getTitle());
        assertEquals(filename, dto.getFilename());
    }

    @Test
    void deleteImage_success() {
        // given
        String title = "Test Title";
        String description = "Test Description";
        String filename = "Test Filename";
        String url = "https://cdn.shopify.com/s/files/1/0272/4770/6214/articles/when-do-puppies-start-walking.jpg";

        Image image = Image.builder()
                .title(title)
                .description(description)
                .filename(filename)
                .url(url)
                .userId("test user id")
                .build();

        given(imageRepository.findByIdAndUserIdAndIsRemoved(anyLong(), anyString(), anyBoolean()))
                .willReturn(Optional.of(image));

        // when
        DeleteImageDto dto = scraperService.deleteImage("0", "userId");

        // then
        assertEquals(DeleteImageDto.class, dto.getClass());
    }
}