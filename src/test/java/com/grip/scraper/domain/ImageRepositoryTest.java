package com.grip.scraper.domain;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class ImageRepositoryTest {

    @Autowired
    ImageRepository imageRepository;

    @Test
    public void saveImage() {
        // given
        String title = "테스트 제목";
        String description = "설명";
        String url = "https://cdn.shopify.com/s/files/1/0272/4770/6214/articles/when-do-puppies-start-walking.jpg";
        String filename = "test_file_name";
        Integer views = 0;

        imageRepository.save(
                Image.builder()
                    .title(title)
                    .description(description)
                    .url(url)
                    .filename(filename)
                    .build()
        );

        // when
        List<Image> imageList = imageRepository.findAll();

        // then
        Image image = imageList.get(0);
        assertEquals(title, image.getTitle());
        assertEquals(description, image.getDescription());
        assertEquals(url, image.getUrl());
        assertEquals(filename, image.getFilename());
        assertEquals(views, image.getViews());
        assertFalse(image.getIsRemoved());
        assertNotNull(image.getCreatedAt());
        assertNotNull(image.getUpdatedAt());
    }
}