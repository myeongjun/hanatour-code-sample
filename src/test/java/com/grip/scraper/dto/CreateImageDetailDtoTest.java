package com.grip.scraper.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CreateImageDetailDtoTest {

    @Test()
    public void createDto() {
        // given
        String title = "테스트 타이틀";
        String description = "테스트 설명";
        String url = "https://cdn.shopify.com/s/files/1/0272/4770/6214/articles/when-do-puppies-start-walking.jpg";

        // when
        CreateImageDto.Request dto = new CreateImageDto.Request(title, description, url);

        // then
        assertEquals(title, dto.getTitle());
        assertEquals(description, dto.getDescription());
        assertEquals(url, dto.getUrl());
    }
}